from pydantic import BaseModel
from typing import Optional


class RedirectIn(BaseModel):
    shortcut: str
    url: str


class RedirectOut(RedirectIn):
    id: int


class RedirectUpdate(RedirectIn):
    shortcut: Optional[str] = None
    url: Optional[str] = None

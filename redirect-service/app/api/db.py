from sqlalchemy import (Column, Integer, MetaData, String, Table,
                        create_engine, ARRAY)

from databases import Database

SQLALCHEMY_DATABASE_URL = "sqlite:///./sql_app.db"
# SQLALCHEMY_DATABASE_URL = "postgresql://user:password@postgresserver/db"

engine = create_engine(
    SQLALCHEMY_DATABASE_URL, connect_args={"check_same_thread": False}
)
metadata = MetaData()

redirects = Table(
    'redirects',
    metadata,
    Column('id', Integer, primary_key=True),
    Column('shortcut', String(50)),
    Column('url', String(250))
)

database = Database(SQLALCHEMY_DATABASE_URL)

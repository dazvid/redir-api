from app.api.models import RedirectIn, RedirectOut, RedirectUpdate
from app.api.db import redirects, database


async def add_redirect(payload: RedirectIn):
    query = redirects.insert().values(**payload.dict())
    return await database.execute(query=query)


async def get_all_redirects():
    query = redirects.select()
    return await database.fetch_all(query=query)


async def get_redirect_by_shortcut(shortcut):
    query = redirects.select(redirects.c.shortcut == shortcut)
    return await database.fetch_one(query=query)


async def get_redirect_by_id(id):
    query = redirects.select(redirects.c.id == id)
    return await database.fetch_one(query=query)


async def delete_redirect(id: int):
    query = redirects.delete().where(redirects.c.id == id)
    return await database.execute(query=query)


async def update_redirect(id: int, payload: RedirectIn):
    query = (
        redirects
        .update()
        .where(redirects.c.id == id)
        .values(**payload.dict())
    )
    return await database.execute(query=query)

from typing import List
from fastapi import APIRouter, HTTPException
from starlette.responses import RedirectResponse

from app.api.models import RedirectIn, RedirectOut, RedirectUpdate
from app.api import db_manager


redirects = APIRouter()


# CREATE
@redirects.post("/", response_model=RedirectOut, status_code=201)
async def create_redirect(payload: RedirectIn):
    redirect_id = await db_manager.add_redirect(payload)
    response = {
        'id': redirect_id,
        **payload.dict()
    }

    return response


# READ
@redirects.get("/", response_model=List[RedirectOut])
async def get_redirects():
    return await db_manager.get_all_redirects()


@redirects.get('/redirect/{shortcut}', status_code=307)
async def get_redirect(shortcut: str):
    redirect = await db_manager.get_redirect_by_shortcut(shortcut)
    if redirect:
        return RedirectResponse(redirect.url)

    return RedirectResponse("https://dazvid.net/")


# UPDATE
@redirects.put('/{id}')
async def update_redirect(id: int, payload: RedirectUpdate):
    redirect = await db_manager.get_redirect_by_id(id)
    if not redirect:
        raise HTTPException(status_code=404, detail="Redirect not found")

    update_data = payload.dict(exclude_unset=True)
    redirect_in_db = RedirectIn(**redirect)
    updated_redirect = redirect_in_db.copy(update=update_data)

    return await db_manager.update_redirect(id, updated_redirect)


# DELETE
@redirects.delete('/{id}', response_model=None)
async def delete_redirect(id: int):
    redirect = await db_manager.get_redirect_by_id(id)
    if not redirect:
        raise HTTPException(status_code=404, detail="Redirect not found")
    return await db_manager.delete_redirect(id)
